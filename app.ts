/**
 * Created with JetBrains WebStorm.
 * User: nakagawa
 * Date: 2013/08/11
 * Time: 11:10
 * To change this template use File | Settings | File Templates.
 */
/// <reference path="ts-definitions/DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="ts-definitions/DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="ts-definitions/DefinitelyTyped/backbone/backbone.d.ts" />

interface TaskOptions {
    title?: string;
    completed?: boolean;
}

class Task extends Backbone.Model {

    constructor(options?:TaskOptions) {
        super();

        // defaultsの代わり
        this.set('title', (options && options.title) ? options.title : "do something");
        this.set('completed', (options && options.completed) ? options.completed : false);
    }

    validate(attributes:any):any {
        if (_.isEmpty(attributes.title)) {
            return 'title must not be empty';
        }
    }

    initialize() {
        this.on('invalid', (model:Task, error:any) => {
            $('#error').html(error);
        });
    }
}

class Tasks extends Backbone.Collection {

    constructor(tasks:Task[]) {
        super(tasks);
        this.model = Task;
    }

}

class TaskView extends Backbone.View {

    template:(...data:any[]) => string;

    constructor(model:Task) {
        super({
            tagName: 'li',
            model: model
        });
        this.template = _.template($('#task-template').html());
    }

    initialize() {
        // constructor内ではeventsの初期化ができない
        this.events = {
            "click .delete": 'destroy',
            "click .toggle": 'toggle'
        };

        this.model.on('destroy', this.remove, this);
        this.model.on('change', this.render, this);
    }

    destroy() {
        if (confirm('are you sure?')) {
            this.model.destroy();
        }
    }

    toggle() {
        this.model.set('completed', !this.model.get('completed'))
    }

    remove():Backbone.View {
        this.$el.remove();
        return this;
    }

    render():Backbone.View {
        var template = this.template(this.model.toJSON());
        this.$el.html(template);
        return this;
    }
}

class TasksView extends Backbone.View {

    constructor(tasks:Tasks) {
        super({
            tagName: 'ul',
            collection: tasks
        });
    }

    initialize() {
        this.collection.on('add', this.addNew, this);
        this.collection.on('change', this.updateCount, this);
        this.collection.on('destroy', this.updateCount, this);
    }

    addNew(task:Task) {
        var taskView = new TaskView(task);
        this.$el.append(taskView.render().el);
        $('#title').val('').focus();
        this.updateCount();
    }

    updateCount() {
        var uncompletedTasks:Backbone.Model[] = this.collection.filter(
            (task:Task):boolean => !task.get('completed')
        );
        $('#count').html(uncompletedTasks.length.toString());
    }

    render():Backbone.View {
        this.collection.each((task:Task) => {
            var taskView = new TaskView(task);
            this.$el.append(taskView.render().el);
        }, this);
        this.updateCount();
        return this;
    }
}

class AddTaskView extends Backbone.View {

    constructor(collection:Backbone.Collection) {
        super({
            el: '#addTask',
            collection: collection
        });
    }

    initialize() {
        this.events = {
            'submit': 'submit'
        }
    }

    submit(e:JQueryEventObject) {
        e.preventDefault();
        var task = new Task();
        if (task.set({title: $('#title').val()}, {validate: true})) {
            this.collection.add(task);
            $('#error').empty();
        }
    }
}

$(() => {
    var tasks = new Tasks([
        new Task({ title: 'task1', completed: true }),
        new Task({ title: 'task2' }),
        new Task({ title: 'task3' })
    ]);

    new AddTaskView(tasks);
    var tasksView = new TasksView(tasks);
    $('#tasks').html(tasksView.render().el);
});