Backbone.js入門の名を借りたTypeScript練習
=========================================

ファイルの説明
--------------

* app.old.js: ドットインストールそのまま
* app.ts: TypeScript版

元ネタ
------

> Backbone.js入門 (全22回) - プログラミングならドットインストール
> [http://dotinstall.com/lessons/basic_backbonejs]()

開発環境
--------

[WebStorm 7 EAP](http://confluence.jetbrains.com/display/WI/WebStorm+7+EAP)

.gitignoreの方針
----------------

1. bower_componentsはbower.jsonさえあれば再取得可能なので、Git管理を行いません
    * でもブランチ毎にバージョンが違う場合、ブランチ変える度にbower updateしないといけない気がするので運用を考えなおしたほうがいいかもしれない
    
2. ts-definitionsはtsd-config.jsonさえあれば再取得可能と言いたいところですが、2013.8.12現在その仕様がまだできていないらしいので、ts-definitionsはGit管理します。

3. TypeScriptコンパイラ(tsc)により生成される.jsファイル、.js.mapファイルはGit管理を行いません。.tsファイルのみをGit管理します。
